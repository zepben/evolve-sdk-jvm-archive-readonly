/*
 * Copyright 2020 Zeppelin Bend Pty Ltd
 * This file is part of evolve-sdk-jvm.
 *
 * evolve-sdk-jvm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * evolve-sdk-jvm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with evolve-sdk-jvm.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.zepben.cimbend.cim.iec61970.base.wires

enum class VectorGroup {

    UNKNOWN,
    DD0,
    DZ0,
    DZN0,
    YNY0,
    YNYN0,
    YY0,
    YYN0,
    ZD0,
    ZND0,
    DYN1,
    DZ1,
    DZN1,
    YD1,
    YND1,
    YNZN1,
    YZ1,
    YZN1,
    ZD1,
    ZND1,
    ZNYN1,
    ZY1,
    ZYN1,
    DY5,
    DYN5,
    YD5,
    YND5,
    YNZ5,
    YNZN5,
    YZ5,
    YZN5,
    ZNY5,
    ZNYN5,
    ZY5,
    ZYN5,
    DD6,
    DZ6,
    DZN6,
    YNY6,
    YNYN6,
    YY6,
    YYN6,
    ZD6,
    ZND6,
    DY7,
    DYN7,
    DZ7,
    DZN7,
    YD7,
    YND7,
    YNZN7,
    YZ7,
    YZN7,
    ZD7,
    ZND7,
    ZNYN7,
    ZY7,
    ZYN7,
    DY11,
    DYN11,
    YD11,
    YND11,
    YNZ11,
    YNZN11,
    YZ11,
    YZN11,
    ZNY11,
    ZNYN11,
    ZY11,
    ZYN11,
    DY1,
    Y0,
    YN0,
    D0,
    ZNY1,
    ZNY7,
    DDN0,
    DND0,
    DNYN1,
    DNYN11,
    YNDN1,
    YNDN11,
    TTN11
}
