/*
 * Copyright 2020 Zeppelin Bend Pty Ltd
 * This file is part of evolve-sdk-jvm.
 *
 * evolve-sdk-jvm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * evolve-sdk-jvm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with evolve-sdk-jvm.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.zepben.cimbend.cim.iec61970.base.wires

import com.zepben.cimbend.common.extensions.asUnmodifiable
import com.zepben.cimbend.common.extensions.getByMRID
import com.zepben.cimbend.common.extensions.validateReference

/**
 * A generic equivalent for an energy supplier on a transmission or distribution voltage level.
 *
 * @property voltageAngle Phase angle of a-phase open circuit used when voltage characteristics need to be imposed at the node associated with
 *                        the terminal of the energy source, such as when voltages and angles from the transmission level are used as input to
 *                        the distribution network. The attribute shall be a positive value or zero.
 * @property voltageMagnitude Phase-to-phase open circuit voltage magnitude used when voltage characteristics need to be imposed at the node
 *                            associated with the terminal of the energy source, such as when voltages and angles from the transmission level
 *                            are used as input to the distribution network. The attribute shall be a positive value or zero.
 * @property pMax This is the maximum active power that can be produced by the source. Load sign convention is used, i.e. positive sign means flow out from a
 *                TopologicalNode (bus) into the conducting equipment.
 * @property pMin This is the minimum active power that can be produced by the source. Load sign convention is used, i.e. positive sign means flow out from a
 *                TopologicalNode (bus) into the conducting equipment.
 * @property r Positive sequence Thevenin resistance.
 * @property r0 Zero sequence Thevenin resistance.
 * @property rn Negative sequence Thevenin resistance.
 * @property x Positive sequence Thevenin reactance.
 * @property x0 Zero sequence Thevenin reactance.
 * @property xn Negative sequence Thevenin reactance.
 */
class EnergySource @JvmOverloads constructor(mRID: String = "") : EnergyConnection(mRID) {

    private var _energySourcePhases: MutableList<EnergySourcePhase>? = null
    var activePower: Double = 0.0
    var reactivePower: Double = 0.0
    var voltageAngle: Double = 0.0
    var voltageMagnitude: Double = 0.0
    var pMax: Double = 0.0
    var pMin: Double = 0.0
    var r: Double = 0.0
    var r0: Double = 0.0
    var rn: Double = 0.0
    var x: Double = 0.0
    var x0: Double = 0.0
    var xn: Double = 0.0

    /**
     * The phases for this energy source. The returned collection is read only.
     */
    val phases: Collection<EnergySourcePhase> get() = _energySourcePhases.asUnmodifiable()

    /**
     * Get the number of entries in the [EnergySourcePhase] collection.
     */
    fun numPhases() = _energySourcePhases?.size ?: 0

    /**
     * The individual phase information of the energy source.
     *
     * @param mRID the mRID of the required [EnergySourcePhase]
     * @return The [EnergySourcePhase] with the specified [mRID] if it exists, otherwise null
     */
    fun getPhase(mRID: String) = _energySourcePhases?.getByMRID(mRID)

    fun addPhase(phase: EnergySourcePhase): EnergySource {
        if (validateReference(phase, ::getPhase, "An EnergySourcePhase"))
            return this

        _energySourcePhases = _energySourcePhases ?: mutableListOf()
        _energySourcePhases!!.add(phase)

        return this
    }

    fun removePhase(phase: EnergySourcePhase?): Boolean {
        val ret = _energySourcePhases?.remove(phase) == true
        if (_energySourcePhases.isNullOrEmpty()) _energySourcePhases = null
        return ret
    }

    fun clearPhases(): EnergySource {
        _energySourcePhases = null
        return this
    }
}
