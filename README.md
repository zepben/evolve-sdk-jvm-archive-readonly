# Evolve SDK #

The Evolve SDK contains everything necessary to communicate with a [Zepben EWB Server](TODO). See the [architecture](docs/architecture.md) documentation for more details.

## Requirements ##

- Java 11

## Building ##

Requirements to build

- JDK11 (tested with Amazon Corretto)
- Maven

To build:

    mvn package


